//find double factorial of the number

function doublefactorial(n) {
    let result = n;
    if(n ===0 || n===1){

        return 1;
    }

    while(n > 2){
        n = n-2;
        result = n*result;
    }
    return result;
}

console.log(doublefactorial(9)); //945


