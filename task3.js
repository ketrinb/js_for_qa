//first example, output 4
let CountTrue = [true, false, false, true, false, true, false, false, true];
let result = CountTrue.filter(i => i === true).length;
console.log(result);

//second example, output 0
let CountTrue = [false, false, false, false];
let result = CountTrue.filter(i => i === true).length;
console.log(result);

//third example, output 0
let CountTrue = [];
let result = CountTrue.filter(i => i === true).length;
console.log(result);
