//create array
array = [{name: "Anna", age: 16},
    {name: "Olena", age: 35},
    {name: "Maria", age: 67},
    {name: "Vasyl", age: 42},
];
//add the 5 object person
array.push({name: "Pavlo", age: 68});
console.log(array)

//a)sort the array in ascending order
const personsAge = [
    {name: "Anna", age: 16},
    {name: "Olena", age: 35},
    {name: "Maria", age: 67},
    {name: "Vasyl", age: 42},
    {name: "Pavlo", age: 68},
]

personsAge.sort(mySort)

function mySort(a, b){
    if(a.age > b.age)
        return 1
    if(a.age < b.age)
        return -1
    return 0
}
console.log(personsAge)

//b)sort the array in descending order
const personsAge = [
    {name: "Anna", age: 16},
    {name: "Olena", age: 35},
    {name: "Maria", age: 67},
    {name: "Vasyl", age: 42},
    {name: "Pavlo", age: 68},
]

personsAge.sort(mySort)

function mySort(a, b){
    if(a.age < b.age)
        return 1
    if(a.age > b.age)
        return -1
    return 0
}
console.log(personsAge)
